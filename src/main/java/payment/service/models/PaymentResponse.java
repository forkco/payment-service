package payment.service.models;

public class PaymentResponse {
    private String token;
    private String statusCode;

    public PaymentResponse(String token, String statusCode) {
        this.token = token;
        this.statusCode = statusCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
