package payment.service.models;

public class PaymentRequest {
    private int amount;
    private String merchantId;
    private String operationTypeCode;
    private String endpoint;

    public PaymentRequest(int amount, String merchantId, String operationTypeCode, String endpoint) {
        this.amount = amount;
        this.merchantId = merchantId;
        this.operationTypeCode = operationTypeCode;
        this.endpoint = endpoint;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getOperationTypeCode() {
        return operationTypeCode;
    }

    public void setOperationTypeCode(String operationTypeCode) {
        this.operationTypeCode = operationTypeCode;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }
}
