package payment.service.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import payment.service.methods.entities.mbway.MBWayClient;
import payment.service.methods.entities.mbway.sibs.financial.RequestFinancialOperationResult;
import payment.service.models.PaymentRequest;
import payment.service.models.PaymentResponse;

@RestController
@RequestMapping("/api")
public class PaymentsController {

    @RequestMapping(value = "/payments", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PaymentResponse paymentRequest(@RequestBody PaymentRequest paymentRequest){
        MBWayClient mbWayClient = new MBWayClient();

        RequestFinancialOperationResult result = mbWayClient.sendPayment(paymentRequest);



        System.out.println(result);

        return new PaymentResponse(result.getToken(), result.getStatusCode());
    }
}
