package payment.service.methods.entities.mbway.constants;

public enum FinancialOperationType {
    PURCHASE                     ("022"),
    PURCHASE_RETURN              ("023"),
    PURCHASE_AUTHORIZATION       ("024"),
    PURCHASE_AFTER_AUTHORIZATION ("025"),
    AUTHORIZATION_CANCEL         ("026"),
    ANNULMENT                    ("048");

    private String value;

    FinancialOperationType(String value) {
        this.value = value;
    }
}
