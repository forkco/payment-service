
package payment.service.methods.entities.mbway.sibs.financial;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for requestFinancialOperationRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="requestFinancialOperationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="messageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aditionalData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="alias" type="{http://financial.services.merchant.channelmanagermsp.sibs/}alias"/>
 *         &lt;element name="financialOperation" type="{http://financial.services.merchant.channelmanagermsp.sibs/}financialOperation"/>
 *         &lt;element name="referencedFinancialOperation" type="{http://financial.services.merchant.channelmanagermsp.sibs/}financialOperation" minOccurs="0"/>
 *         &lt;element name="merchant" type="{http://financial.services.merchant.channelmanagermsp.sibs/}merchant"/>
 *         &lt;element name="messageProperties" type="{http://financial.services.merchant.channelmanagermsp.sibs/}messageProperties"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestFinancialOperationRequest", propOrder = {
    "messageType",
    "aditionalData",
    "alias",
    "financialOperation",
    "referencedFinancialOperation",
    "merchant",
    "messageProperties"
})
public class RequestFinancialOperationRequest {

    @XmlElement(required = true, defaultValue = "N0003")
    protected String messageType;
    protected String aditionalData;
    @XmlElement(required = true)
    protected Alias alias;
    @XmlElement(required = true)
    protected FinancialOperation financialOperation;
    protected FinancialOperation referencedFinancialOperation;
    @XmlElement(required = true)
    protected Merchant merchant;
    @XmlElement(required = true)
    protected MessageProperties messageProperties;

    /**
     * Gets the value of the messageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * Sets the value of the messageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageType(String value) {
        this.messageType = value;
    }

    /**
     * Gets the value of the aditionalData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAditionalData() {
        return aditionalData;
    }

    /**
     * Sets the value of the aditionalData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAditionalData(String value) {
        this.aditionalData = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link Alias }
     *     
     */
    public Alias getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link Alias }
     *     
     */
    public void setAlias(Alias value) {
        this.alias = value;
    }

    /**
     * Gets the value of the financialOperation property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialOperation }
     *     
     */
    public FinancialOperation getFinancialOperation() {
        return financialOperation;
    }

    /**
     * Sets the value of the financialOperation property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialOperation }
     *     
     */
    public void setFinancialOperation(FinancialOperation value) {
        this.financialOperation = value;
    }

    /**
     * Gets the value of the referencedFinancialOperation property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialOperation }
     *     
     */
    public FinancialOperation getReferencedFinancialOperation() {
        return referencedFinancialOperation;
    }

    /**
     * Sets the value of the referencedFinancialOperation property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialOperation }
     *     
     */
    public void setReferencedFinancialOperation(FinancialOperation value) {
        this.referencedFinancialOperation = value;
    }

    /**
     * Gets the value of the merchant property.
     * 
     * @return
     *     possible object is
     *     {@link Merchant }
     *     
     */
    public Merchant getMerchant() {
        return merchant;
    }

    /**
     * Sets the value of the merchant property.
     * 
     * @param value
     *     allowed object is
     *     {@link Merchant }
     *     
     */
    public void setMerchant(Merchant value) {
        this.merchant = value;
    }

    /**
     * Gets the value of the messageProperties property.
     * 
     * @return
     *     possible object is
     *     {@link MessageProperties }
     *     
     */
    public MessageProperties getMessageProperties() {
        return messageProperties;
    }

    /**
     * Sets the value of the messageProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageProperties }
     *     
     */
    public void setMessageProperties(MessageProperties value) {
        this.messageProperties = value;
    }

}
