package payment.service.methods.entities.mbway.constants;

public enum Currency {
    EURO_TWO_DECIMALS ("9782");

    private String value;

    Currency(String value) {
        this.value = value;
    }
}
