
package payment.service.methods.entities.mbway.sibs.financial;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the entities.mbway.sibs.channelmanagermsp.merchant.services.financial package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RequestFinancialOperationResponse_QNAME = new QName("http://financial.services.merchant.channelmanagermsp.sibs/", "requestFinancialOperationResponse");
    private final static QName _RequestFinancialOperation_QNAME = new QName("http://financial.services.merchant.channelmanagermsp.sibs/", "requestFinancialOperation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: entities.mbway.sibs.channelmanagermsp.merchant.services.financial
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RequestFinancialOperationResponse }
     * 
     */
    public RequestFinancialOperationResponse createRequestFinancialOperationResponse() {
        return new RequestFinancialOperationResponse();
    }

    /**
     * Create an instance of {@link RequestFinancialOperation }
     * 
     */
    public RequestFinancialOperation createRequestFinancialOperation() {
        return new RequestFinancialOperation();
    }

    /**
     * Create an instance of {@link MessageProperties }
     * 
     */
    public MessageProperties createMessageProperties() {
        return new MessageProperties();
    }

    /**
     * Create an instance of {@link RequestFinancialOperationRequest }
     * 
     */
    public RequestFinancialOperationRequest createRequestFinancialOperationRequest() {
        return new RequestFinancialOperationRequest();
    }

    /**
     * Create an instance of {@link RequestFinancialOperationResult }
     * 
     */
    public RequestFinancialOperationResult createRequestFinancialOperationResult() {
        return new RequestFinancialOperationResult();
    }

    /**
     * Create an instance of {@link Alias }
     * 
     */
    public Alias createAlias() {
        return new Alias();
    }

    /**
     * Create an instance of {@link Merchant }
     * 
     */
    public Merchant createMerchant() {
        return new Merchant();
    }

    /**
     * Create an instance of {@link FinancialOperation }
     * 
     */
    public FinancialOperation createFinancialOperation() {
        return new FinancialOperation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestFinancialOperationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://financial.services.merchant.channelmanagermsp.sibs/", name = "requestFinancialOperationResponse")
    public JAXBElement<RequestFinancialOperationResponse> createRequestFinancialOperationResponse(RequestFinancialOperationResponse value) {
        return new JAXBElement<RequestFinancialOperationResponse>(_RequestFinancialOperationResponse_QNAME, RequestFinancialOperationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestFinancialOperation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://financial.services.merchant.channelmanagermsp.sibs/", name = "requestFinancialOperation")
    public JAXBElement<RequestFinancialOperation> createRequestFinancialOperation(RequestFinancialOperation value) {
        return new JAXBElement<RequestFinancialOperation>(_RequestFinancialOperation_QNAME, RequestFinancialOperation.class, null, value);
    }

}
