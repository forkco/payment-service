
package payment.service.methods.entities.mbway.com.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import payment.service.methods.entities.mbway.sibs.financial.RequestFinancialOperationResult;


/**
 * <p>Java class for financialOperationResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="financialOperationResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://financial.services.merchant.channelmanagermsp.sibs/}requestFinancialOperationResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "financialOperationResult", propOrder = {
    "arg0"
})
public class FinancialOperationResult {

    protected RequestFinancialOperationResult arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link RequestFinancialOperationResult }
     *     
     */
    public RequestFinancialOperationResult getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestFinancialOperationResult }
     *     
     */
    public void setArg0(RequestFinancialOperationResult value) {
        this.arg0 = value;
    }

}
