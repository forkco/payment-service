package payment.service.methods.entities.mbway.constants;

public enum MessageType {
    CREATE_MERCHANT_ALIAS ("N0001"),
    REMOVE_MERCHANT_ALIAS ("N0002"),
    FINANCIAL_OPERATION   ("N0003");

    private String value;

    MessageType(String value) {
        this.value = value;
    }
}
