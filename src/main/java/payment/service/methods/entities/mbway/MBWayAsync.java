package payment.service.methods.entities.mbway;

import javax.xml.ws.Endpoint;

public class MBWayAsync extends Thread{

    private String endpoint;

    public MBWayAsync(String endpoint){
        this.endpoint = endpoint;
    }

    public void run() {
        AsyncService as = new AsyncService();
        //as.setEndpoint(this.endpoint);

        System.out.println("Starting service...");

        String address = "http://forkittest.herokuapp.com/FinancialOperationAsyncResult";

        Endpoint.publish(address, as);
    }
}
