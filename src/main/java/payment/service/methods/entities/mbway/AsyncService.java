package payment.service.methods.entities.mbway;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import payment.service.methods.entities.mbway.com.webservices.FinancialOperationAsyncResult;
import payment.service.methods.entities.mbway.sibs.financial.RequestFinancialOperationResult;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@Component(value = "financialOperationAsyncResultService")
@WebService(
    name = "FinancialOperationAsyncResult",
    targetNamespace = "http://webservices.sibsmerchant.com/"
)
public class AsyncService extends SpringBeanAutowiringSupport implements FinancialOperationAsyncResult {

    @WebMethod
    @RequestWrapper(
        localName = "financialOperationResult",
        targetNamespace = "http://webservices.sibsmerchant.com/",
        className = "FinancialOperationResult"
    )
    @ResponseWrapper(
        localName = "financialOperationResultResponse",
        targetNamespace = "http://webservices.sibsmerchant.com/",
        className = "FinancialOperationResultResponse"
    )
    @Action(
        input = "http://webservices.sibsmerchant.com/FinancialOperationAsyncResult/financialOperationResultRequest",
        output = "http://webservices.sibsmerchant.com/FinancialOperationAsyncResult/financialOperationResultResponse"
    )
    public void financialOperationResult( @WebParam(name = "arg0", targetNamespace = "") RequestFinancialOperationResult arg0){
        System.out.println("------");
        System.out.println(arg0);
        System.out.println("------");

        try {
            System.out.println(arg0);
            String postUrl = "https://forkittest.gigalixirapp.com/api/mbway/payment-confirmation";

            Gson gson = new Gson();
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPatch patch = new HttpPatch(postUrl);

            StringEntity postingString = null;

            postingString = new StringEntity(gson.toJson(arg0));

            patch.setEntity(postingString);
            patch.setHeader("Content-type", "application/json");

            System.out.println("---response---");
            HttpResponse response = httpClient.execute(patch);
            System.out.println("---response---");
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("------");
    }
}
