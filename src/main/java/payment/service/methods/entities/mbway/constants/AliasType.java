package payment.service.methods.entities.mbway.constants;

public enum AliasType {
    CELLPHONE ("001"),
    EMAIL     ("002"),
    GENERIC   ("005");

    private String value;

    AliasType(String value) {
        this.value = value;
    }
}
