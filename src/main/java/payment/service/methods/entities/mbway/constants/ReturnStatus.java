package payment.service.methods.entities.mbway.constants;

public enum ReturnStatus {
    SUCCESS                           ("000"),
    REJECTED_CREATE_ALIAS             ("010"),
    REJECTED_FINANCIAL_OPERATION      ("020"),
    ERROR                             ("100"),
    ERROR_TIMEOUT                     ("101"),
    ERROR_VALIDATION                  ("102"),
    ERROR_INVALID_POS                 ("103"),
    ERROR_DENIED                      ("104"),
    ALIAS_TYPE_BLOCKED                ("110"),
    ALIAS_INVALID                     ("111"),
    ALIAS_DUPLICATED                  ("112"),
    ALIAS_UNKNOWN                     ("113"),
    FINANCIAL_OPERATION_DUPLICATED    ("120"),
    FINANCIAL_OPERATION_TYPE_NOTFOUND ("121"),
    FINANCIAL_OPERATION_DENIED        ("122"),
    FINANCIAL_OPERATION_NOTFOUND      ("123"),
    FINANCIAL_OPERATION_ALIAS_DENIED  ("124");

    private String value;

    ReturnStatus(String value) {
        this.value = value;
    }
}
