package payment.service.methods.entities.mbway.constants;

public enum Channel {
    MOBILE        ("01"),
    TV            ("02"),
    WEB           ("03"),
    VENDING       ("04"),
    POINT_OF_SALE ("05");

    private String value;

    Channel(String value) {
        this.value = value;
    }
}
