
package payment.service.methods.entities.mbway.com.webservices;

import payment.service.methods.entities.mbway.sibs.financial.RequestFinancialOperationResult;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "FinancialOperationAsyncResult", targetNamespace = "http://webservices.sibsmerchant.com/")
@XmlSeeAlso({
    ObjectFactory.class,
    payment.service.methods.entities.mbway.sibs.financial.ObjectFactory.class
})
public interface FinancialOperationAsyncResult {


    /**
     * 
     * @param arg0
     */
    @WebMethod
    @RequestWrapper(localName = "financialOperationResult", targetNamespace = "http://webservices.sibsmerchant.com/", className = "FinancialOperationResult")
    @ResponseWrapper(localName = "financialOperationResultResponse", targetNamespace = "http://webservices.sibsmerchant.com/", className = "FinancialOperationResultResponse")
    @Action(input = "http://webservices.sibsmerchant.com/FinancialOperationAsyncResult/financialOperationResultRequest", output = "http://webservices.sibsmerchant.com/FinancialOperationAsyncResult/financialOperationResultResponse")
    public void financialOperationResult(
        @WebParam(name = "arg0", targetNamespace = "")
        RequestFinancialOperationResult arg0);

}
