package payment.service.methods.entities.mbway;

import payment.service.methods.entities.Method;
import payment.service.methods.entities.mbway.constants.*;
import payment.service.methods.entities.mbway.sibs.financial.*;
import payment.service.models.PaymentRequest;

public class MBWayClient implements Method {
    private static final String HOST = "http://forkittest.herokuapp.com/";
    private static final String ENDPOINT = "FinancialOperationAsyncResult";

    public RequestFinancialOperationResult sendPayment(PaymentRequest paymentRequest){
        //runAsyncService(paymentRequest.getEndpoint());

        debug(true);

        MerchantFinancialOperationWSService service = new MerchantFinancialOperationWSService();
        MerchantFinancialOperationWS merchantfinancialOperation = service.getMerchantFinancialOperationWSPort();

        FinancialOperation financialOperation = new FinancialOperation();
        financialOperation.setAmount(paymentRequest.getAmount());
        financialOperation.setCurrencyCode(Currency.EURO_TWO_DECIMALS.toString());
        financialOperation.setMerchantOprId(paymentRequest.getMerchantId());
        financialOperation.setOperationTypeCode(paymentRequest.getOperationTypeCode());

        RequestFinancialOperationRequest request = buildMessage(financialOperation);

        String action = buildAction(service);

        ReplyTo replyto = new ReplyTo();
        replyto.setAddress(HOST + ENDPOINT);

        return merchantfinancialOperation.requestFinancialOperation(action, replyto, request);
    }


    private void runAsyncService(String endpoint){
        MBWayAsync service = new MBWayAsync(endpoint);
        service.start();
    }

    private void debug(boolean value){
        if(value){
            System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        }
    }

    private String buildAction(MerchantFinancialOperationWSService service){
        String host = service.getWSDLDocumentLocation().getAuthority();

        String action = "http://" + host + "/" + ENDPOINT;

        return action;
    }

    private RequestFinancialOperationRequest buildMessage(FinancialOperation financialOperation){
        RequestFinancialOperationRequest request = new RequestFinancialOperationRequest();

        Alias alias = new Alias();
        alias.setAliasTypeCde(AliasType.CELLPHONE.toString());
        alias.setAliasName("351#913519801");

        Merchant merchant = new Merchant();
        merchant.setIPAddress("255.255.255.255");
        merchant.setPosId("24162");

        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setApiVersion("1");
        messageProperties.setChannel(Channel.MOBILE.toString());
        messageProperties.setChannelTypeCode("VPOS");
        messageProperties.setNetworkCode("MULTIB");
        messageProperties.setServiceType("01");

        request.setAlias(alias);
        request.setFinancialOperation(financialOperation);
        request.setMerchant(merchant);
        request.setMessageProperties(messageProperties);
        request.setMessageType(MessageType.FINANCIAL_OPERATION.toString());

        return request;
    }
}
