
package payment.service.methods.entities.mbway.com.webservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sibsmerchant.webservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FinancialOperationResult_QNAME = new QName("http://webservices.sibsmerchant.com/", "financialOperationResult");
    private final static QName _FinancialOperationResultResponse_QNAME = new QName("http://webservices.sibsmerchant.com/", "financialOperationResultResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sibsmerchant.webservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FinancialOperationResult }
     * 
     */
    public FinancialOperationResult createFinancialOperationResult() {
        return new FinancialOperationResult();
    }

    /**
     * Create an instance of {@link FinancialOperationResultResponse }
     * 
     */
    public FinancialOperationResultResponse createFinancialOperationResultResponse() {
        return new FinancialOperationResultResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancialOperationResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.sibsmerchant.com/", name = "financialOperationResult")
    public JAXBElement<FinancialOperationResult> createFinancialOperationResult(FinancialOperationResult value) {
        return new JAXBElement<FinancialOperationResult>(_FinancialOperationResult_QNAME, FinancialOperationResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancialOperationResultResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://webservices.sibsmerchant.com/", name = "financialOperationResultResponse")
    public JAXBElement<FinancialOperationResultResponse> createFinancialOperationResultResponse(FinancialOperationResultResponse value) {
        return new JAXBElement<FinancialOperationResultResponse>(_FinancialOperationResultResponse_QNAME, FinancialOperationResultResponse.class, null, value);
    }
}
