package payment.service.methods.entities;

import payment.service.methods.entities.mbway.sibs.financial.RequestFinancialOperationResult;
import payment.service.models.PaymentRequest;

public interface Method {
    RequestFinancialOperationResult sendPayment(PaymentRequest paymentRequest);
}
